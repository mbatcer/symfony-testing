<?php
namespace AppBundle\Services;

use SoapClient;

class TrainAPI
{
    private $soapUrl;
    
    public function __construct($url) {
        $this->soapUrl = $url;
    }

    public function call($soapFunction, $soapParams) {
        $auth = array(
            'auth' => array(
                'login' => 'test',
                'psw' => 'bYKoDO2it',
                'terminal' => 'htk_test',
                'represent_id' => '22400'
            )
        );
        $soapParams = $auth + $soapParams;
        $soapClient = new SoapClient($this->soapUrl);
        $result = $soapClient->__soapCall($soapFunction, $soapParams);
        return $result;
    }
}

