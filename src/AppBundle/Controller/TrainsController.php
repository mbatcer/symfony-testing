<?php
namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class TrainsController extends Controller
{
    public function indexAction()
    {
        return $this->render('index.html.twig');
    }

    public function travelInfoAction($train)
    {
        $request = $this->getRequest();
        if ($request->getMethod() !== Request::METHOD_GET) {
            // Здесь можно было бы добавить более конкретное сообщение
            return $this->errorResponse(
                    'Incorrect request', Response::HTTP_BAD_REQUEST);
        }
        $query = $request->query;
        $params = array(
            $train,
            'trainTravelInfo' => array(
                'from' => $query->get('from') ? $query->get('from') : '',
                'to' => $query->get('to') ? $query->get('to') : '',
                'day' => $query->get('day') ? $query->get('day') : '',
                'month' => $query->get('month') ? $query->get('month') : ''
            )
        );
        try {
            $result = $this->get('TrainAPI')->call('trainRoute', $params);
        }
        catch(\SoapFault $e) {
            // Здесь можно было бы добавить более конкретное сообщение
            return $this->errorResponse(
                    'Incorrect request', Response::HTTP_BAD_REQUEST);
        }
        return $this->jsonResponse($result);
    }
    
    private function errorResponse($message, $statusCode) {
        $result = array(
            'success' => 'false',
            'message' => $message
        );
        $response = $this->jsonResponse($result);
        $response->setStatusCode($statusCode);
        return $response;
    }
    
    private function jsonResponse($content) {
        $response = new Response(json_encode($content));
        $response->headers->set('Content-Type', 'application/json; charset=utf-8');
        return $response;
    }
}

